This image starts a nginx on `192.168.5.5` and starts tcpdump to show
incoming/outgoing Ethernet packets.

`start.sh` redirects `127.0.0.1:8090` on the host to nginx inside the guest:
<http://127.0.0.1:8090/>.
