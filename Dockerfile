FROM gcc as env

RUN apt-get update && apt-get upgrade -y && apt-get -y install \
  cpio \
  unzip \
  rsync \
  bc

WORKDIR /root
COPY buildroot buildroot
COPY external-packages external-packages

WORKDIR /root/buildroot
RUN make BR2_EXTERNAL="/root/external-packages" BR2_JLEVEL="$(nproc)" menuconfig || true

COPY .config .
RUN make BR2_EXTERNAL="/root/external-packages" BR2_JLEVEL="$(nproc)" oldconfig

from env
RUN make BR2_EXTERNAL="/root/external-packages" BR2_JLEVEL="$(nproc)"

RUN cp output/images/disk.img /opt/

# FROM scratch
# COPY --from=0 /root/buildroot/output/images/rootfs.iso9660 /image.iso
